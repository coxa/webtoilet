<?php
/* @var $this yii\web\View */
use miloschuman\highcharts\Highcharts;
use yii\helpers\Url;

$this->title = 'Minsk Toilet';
$closedStatusText = '<i class="fa fa-minus-circle fa-5x"></i>';
$openStatusText = '<i class="fa fa-check-circle fa-5x"></i>';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Поздравляю!</h1>

        <p class="lead">Теперь тут будут появляться всякие интересные штуки про туалет.</p>

<?php
    $toiletStatus = \Yii::$app->toilet->getStatus();
?>

        <p><a id="btnStatus" style="pointer-events: none;" class="btn btn-lg btn-<?= $toiletStatus?'danger':'success' ?>" href="#"><?= $toiletStatus? $closedStatusText : $openStatusText ?></a></p>
        <script>
            var timeout = 15;
            var currentValue = 15;
            var currentStatus = <?= $toiletStatus?'true':'false' ?>;
            var timer= null;
            var faviconFree = 'favicon-free.ico';
            var faviconClosed = 'favicon-closed.ico';
            var versionService = <?= \Yii::$app->toilet->getVersion(); ?>;

            document.head || (document.head = document.getElementsByTagName('head')[0]);

            function changeFavicon(src) {
                var link = document.createElement('link'),
                    oldLink = document.getElementById('dynamic-favicon');
                link.id = 'dynamic-favicon';
                link.rel = 'shortcut icon';
                link.href = src;
                if (oldLink) {
                    document.head.removeChild(oldLink);
                }
                document.head.appendChild(link);
            }

            function startTimer(){
                timer = setInterval(function(){
                    if(currentValue == 1){
                        updateStatus();
                        currentValue = timeout;
                    }else{
                        currentValue --;
                    }

                    //update progressbar
                    var percent = currentValue*100/timeout;
                    $('#progress').attr('aria-valuemax', timeout).attr('aria-valuenow', currentValue).css('width', percent + '%');
                }, 500);
            }

            startTimer();

            function updateStatus(){
                timeout = null;
                $.ajax({
                    url: '<?= Url::toRoute('api/status'); ?>',
                    dataType: 'json',
                    beforeSend: function(){
                        timeout = setTimeout(function(){
                            $('#btnStatus').html('<i class="fa fa-refresh fa-spin  fa-5x"></i>');
                        }, 100);
                        clearInterval(timer);
                    },
                    success: function(data){
                        //data.status = (Math.floor(Math.random() * 2) + 0) == 1;
                        //data.status
                        if(data.version > versionService){
                            window.location.reload();
                            return false;
                        }
                        var classBtn = 'btn-success';
                        var textBtn = '<?= $openStatusText; ?>';
                        var fav = faviconFree;
                        if(data.status) {
                            classBtn = 'btn-danger';
                            textBtn = '<?= $closedStatusText; ?>';
                            fav = faviconClosed;
                        }
                        clearTimeout(timeout);
                        $('#btnStatus').removeClass('btn-danger').removeClass('btn-success').addClass(classBtn).html(textBtn);

                        $('#visitorsToday').html(data.visits_today);
                        changeFavicon('/'+fav);
                    },
                    complete: function(){
                        startTimer();
                    }
                });
            }
        </script>
        <div class="progress">
            <div id="progress" class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" >
            </div>
        </div>

        <p>За сегодня количество посещений туалета: <strong id="visitorsToday"><?= \Yii::$app->toilet->getVisitsToday(); ?></strong></p>

    </div>

</div>
