<?php
use miloschuman\highcharts\Highcharts;
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'Туалетная статистика';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php
        $data = \app\models\ToiletHistory::getAvarageVisitsPerWeekDay();
        $days = array_keys($data);
        $week=array(0=>"вс", "пн","вт","ср","чт","пт","сб");
        $dataValues = [];

        foreach($days as &$day){
            $dataValues[] = $data[$day];
            $day = $week[$day];
        }

        echo Highcharts::widget([
            'options' => [
                'title' => ['text' => 'Средняя посещаемость туалета по дням'],
                'chart' => ['type' => 'column'],
                'xAxis' => [
                    'categories' => $days
                ],
                'yAxis' => [
                    'title' => ['text' => 'Количество посещений']
                ],
                'series' => [
                    ['name' => 'Посещения', 'data' => $dataValues]
                ]
            ]
        ]);
        ?>
    </p>
</div>
