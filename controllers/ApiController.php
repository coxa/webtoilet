<?php
/**
 * webtoilet
 * User: Nick Sahnovsky
 * Date: 21.10.14
 * Time: 11:33
 */

namespace app\controllers;


use app\components\Toilet;
use yii\web\Controller;

class ApiController extends Controller{

    public function actionStatus(){
        /** @var Toilet $Toilet */
        $Toilet = \Yii::$app->toilet;
        $data = ['status' => $Toilet->getStatus()];
        $data['visits_today'] = $Toilet->getVisitsToday();
        $data['version'] = $Toilet->getVersion();

        \Yii::$app->getResponse()->data = $data;
        \Yii::$app->getResponse()->setStatusCode(200);
        \Yii::$app->getResponse()->format = 'json';
        \Yii::$app->getResponse()->send();
    }

}
