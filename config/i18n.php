<?php
return [
    'translations' => [
        'toilet' => [
            'class' => 'yii\i18n\PhpMessageSource',
            //'basePath' => '@app/messages',
            //'sourceLanguage' => 'en-US',
            'fileMap' => [
                'toilet' => 'toilet.php',
                'toilet/error' => 'error.php',
            ],
        ],
    ]
];
