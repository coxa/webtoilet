<?php
/**
 * webtoilet
 * User: Nick Sahnovsky
 * Date: 21.10.14
 * Time: 11:47
 */

namespace app\components;


use app\models\ToiletHistory;
use Yii;

class Toilet {

    const TOILET_STATUS_KEY = 'toiletStatus';
    const TOILET_VISITS_TODAY = 'toiletVisitsToday';
    const SERVICE_VERSION = 1;

    /**
     * @param $status
     * @return bool
     */
    public function setStatus($status){
        $status = $status === true;
        $result = false;

        if(Yii::$app->cache->set(self::TOILET_STATUS_KEY, $status)) {
            $history = new ToiletHistory();
            $history->status = $status;
            $result = $history->save();

            $this->updateVisitsToday();
        }

        return $result;
    }

    /**
     * @return boolean
     */
    public function getStatus(){
        return Yii::$app->cache->get(self::TOILET_STATUS_KEY);
    }

    /**
     * @return int|mixed
     */
    public function getVisitsToday(){
        if ( Yii::$app->cache->exists(self::TOILET_VISITS_TODAY) ) {
            $result = Yii::$app->cache->get(self::TOILET_VISITS_TODAY);
        } else {
            $result = $this->updateVisitsToday();
        }
        return $result;
    }

    private function updateVisitsToday(){
        $result = ToiletHistory::calculateVisitsToday();
        Yii::$app->cache->set(self::TOILET_VISITS_TODAY, $result);
        return $result;
    }

    /**
     * @return float
     */
    public function getVersion(){
        return self::SERVICE_VERSION;
    }
}
