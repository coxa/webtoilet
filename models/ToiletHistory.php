<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "toilet_history".
 *
 * @property integer $id
 * @property boolean $status
 * @property string $created
 */
class ToiletHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'toilet_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'boolean'],
            [['created'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('toilet', 'ID'),
            'status' => Yii::t('toilet', 'Status'),
            'created' => Yii::t('toilet', 'Created'),
        ];
    }

    /**
     * @return int|mixed
     */
    static function calculateVisitsToday(){
        $today = date('Y-m-d');
        return self::calculateVisitsByDate($today);
    }

    /**
     * @param string $date Y-m-d
     * @return int|mixed
     */
    static function calculateVisitsByDate($date){
        $x = self::find()->select('count(\'id\') as id')->where([
            '(created::date)' => $date,
            'status' => true
        ])->groupBy('(created::date)')->one();
        return is_null($x)?0:$x->id;
    }

    static function getWeeksDates($year = null){
        if(is_null($year)){
            $year = date('Y');
        }
        $sql = "SELECT to_date( to_char(EXTRACT(WEEK FROM created):: bigint, '99') || ' $year' , 'IW IYYY') as week_begin, to_date( to_char(EXTRACT(WEEK FROM created):: bigint, '99') || ' $year' , 'IW IYYY')  + 6 as week_end  FROM toilet_history
WHERE status GROUP BY week_begin;";

        $weeks = self::getDb()->createCommand($sql )->cache(60*60*24)->queryAll();
        return $weeks;
    }

    /**
     * @return array
     */
    static function getAvarageVisitsPerWeekDay(){
        $sql = "SELECT count(id), EXTRACT(ISODOW FROM created) as dow
  FROM toilet_history
WHERE
	status
	AND created BETWEEN :week_begin AND :week_end
  GROUP BY dow
  ORDER BY dow;";

        $weeks = self::getWeeksDates();
        $result = [];

        foreach($weeks as $week){
            $daysCount = self::getDb()->createCommand($sql, [':week_begin' => $week['week_begin'], ':week_end' => $week['week_end']])->cache(60*60*24)->queryAll();

            foreach($daysCount as $dow){
                $result[  $dow['dow']  ][] = $dow['count'];
            }

        }

        foreach($result as $key => &$value){
            $value = array_sum($value) / count($value);
        }

        return $result;
    }
}
