<?php

use yii\db\Schema;
use yii\db\Migration;

class m141016_204543_create_table_toilet_history extends Migration
{
    public function up()
    {
        $this->createTable('toilet_history', [
            'id' => 'pk',
            'status' => Schema::TYPE_BOOLEAN . ' DEFAULT FALSE NOT NULL',
            'created' => Schema::TYPE_TIMESTAMP . ' default current_timestamp NOT NULL'
        ]);
    }

    public function down()
    {
        $this->dropTable('toilet_history');
    }
}
